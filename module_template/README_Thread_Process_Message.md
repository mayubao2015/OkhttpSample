# Android 进程,线程与消息通信

## Intent与Android基础组件通信传递大数据(binder transaction的缓存为1MB)，可能导致OOM
## 多进程时，如果有自定的Application时需要避免多次创建，避免加载一些主进程三方业务等
## 新建线程时，必须通过线程池提供（AsyncTask或者ThreadPollExecutor或者其他形式自定义的线程池）
## 线程池不允许使用Executors去创建，而是通过ThreadPoolExecutor的方式去创建，这样让其他开发人员更了解线程池的配置
## 子线程不要更新界面，更新界面必须在主线程中，网络操作不能在主线程
## 尽量减少不同App之间的进程间通信及拉起行为，拉起导致占用系统资源，影响用户体验
## 新建线程时，定义能是被自己业务的线程名称，便于性能优化以及问题排查
## ThreadPoolExecutor设置线程存活时间（setKeepAliveTime）,确保空闲时线程能被释放
## 禁止多进程之间使用SharedPreferences共享数据
## 谨慎使用Android的多进程，多进程虽然能够降低主进程的内存压力，但会遇到一下问题
    1）Application实例化多次
    2）进入新进程页面可能会有延时的现象














