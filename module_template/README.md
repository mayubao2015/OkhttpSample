# module_template
This is a template module for project that is component-based.

## 项目介绍
module_template 是组件化，模块化项目的命名规范module
主要包括一下的命名规范：
 - Android资源文件命名以及使用    --->>>具体查看module_template的命名
 - Android基本组件                --->>>具体查看src\main\java\io\github\mayubao\module_template中存在XXX.java的命名以及comment
 - UI与布局                --->>> README_Android_UI_Layout.md
 - 进程，线程与消息通信    --->>> README_Thread_Process_Message.md
 - 文件与数据库            --->>> README_File_DB.md
 - Bitmap, Drawable 与动画 --->>> README_Bitmap_Drawable_Animation.md
 - 项目编码采用 UTF-8


## 遇到的问题








