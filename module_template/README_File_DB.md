# Android 文件，数据库

## 任何时候不要使用硬编码文件路径，使用Android文件系统API访问
## 使用外部存储，必须检查外部存储的可用性
## SharedPreference只记录简单的数据类型，复杂数据类型文件或者数据库记录
## SharedPreference的Editor.apply是缓存数据到内存，再提交到文件中，
Editor.commit()是直接写到文件
## 数据库Cursor必须确保使用完后关闭，避免内存泄漏
## 多线程操作写数据时，需要使用事务，以免出现同步问题
    单个App中使用SQLiteOpenHelperde的单例模式可以确保数据库连接唯一
    SQLite自身是数据库级锁，单个数据库操作保证线程安全，不能保证多线程写入的问题，所以使用事务可以避免这个问题

## 大数据写入数据库时，请使用或其他能够提高IO效率的机制，保证执行速度
## 执行SQL语句时，应该使用SQLiteDatabase#insert(),update(), delete(), 不要使用SQLiteDatabase#execSQL(),以免SQL注入风险
## 如果ContentProvider管理的数据存储在SQL数据库中， 应该避免不受信任的外部数据直接拼接在原始的SQL语句中















