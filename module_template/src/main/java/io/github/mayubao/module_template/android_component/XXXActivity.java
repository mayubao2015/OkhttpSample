package io.github.mayubao.module_template.android_component;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;

/**
 * 1.Activity之间的数据通信，对于数据量比较大的，避免使用Intent+Parcelable方式，可以考虑使用EventBus来代替方案，以免造成TransactionTooLargeException
 * 2.持久化存储应该在onPause/onStop方法，不应该在onSaveInstanceState,onSaveInstanceState方法只能保存临时数据
 * 3.隐士的Activity跳转一定要resolveActivity检查，避免找不到合适的调用组件，造成ActivityNotFoundException异常
 * //TODO　？？不是很明白
 * 4.不要在onDestroy()去执行资源释放的工作，如一些工作线程的销毁和停止，因为onDestroy()执行的时机会比较晚。可根据实际需要，在Activity#onPause()/onStop()中结合isFinishing()的判断来执行
 * 5.如非必须，避免使用嵌套的Fragment
 * 6.使用显示的Intent启动或者绑定Servcie,保证应用安全性
 * 7.当前的Activity的onPause方法执行结束后才会创建onCreate或者onRestart别的Activity, onPause()不适合做耗时的工作，会影响到页面跳转的效率
 *
 * 总结：不要在Activity的重要生命周期方法做耗时任务，会影响UI显示，由于重要的生命周期方法都是在主线程，一般来说不适合在主线程中去耗时任务
 * 重要的生命周期方法一般有：
 * Activity#onCreate(),Activity#onStart(),Activity#onResume()...
 */
public class XXXActivity extends Activity {

    /**
     * 意外销毁UI保存的，保存临时数据 跟数据的持久化存储是不同的概念
     * @param outState
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }


    /**
     * 测试Activity跳转
     */
    public void testStartActivity(){
        Intent intent = new Intent(Intent.ACTION_VIEW);
        if(getPackageManager().resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY) != null){
            startActivity(intent);
        }else{
            //找不到指定的Activity
        }
    }
}
