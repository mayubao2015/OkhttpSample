package io.github.mayubao.module_template.android_ui_layout;

import android.support.v4.app.DialogFragment;

/**
 * Activity中显示对话框或者弹出浮层时，尽量使用DialogFragment，而非Dialog/AlertDialog,因为DialogFragment便于随Activity生命周期
 */
public class XXXDialogFragment extends DialogFragment {
    public XXXDialogFragment() {
        super();
    }
}
