package io.github.mayubao.module_template;

import android.app.Application;

/**
 * 全局的上下文
 */
public class AppContext extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
    }
}
