package io.github.mayubao.module_template.android_ui_layout;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

/**
 * Android上面的任何一个View都是要经过measure， layout, draw这三个步骤才能被真确的渲染。
 * 如果是xml的话，首先会先解析xml转换成对应的Component,然后对layout的顶部节点开始measure,每个子节点向自己的父节点提供自己的
 * 尺寸来决定展示的位置，在此过程中还会重新measure(由此可能导致measure的时间消耗为原来的2-3倍)
 * 如果节点的所处位置越深，嵌套带来的measure越多，计算就会越费时，这就是为什扁平的View结构性能会比较好。
 * 同时，页面上面的View越多，measure, layout， draw所花费的时间就越久，要缩短这个时间，关键是令这个View足够的扁平化，移除不必要的View
 *
 *
 * 理想情况下： View的measure, layout, draw时间应该很好的控制在16ms以内，保证滑动屏幕时UI的流畅
 *
 * 解决方案：可以使用ConstraintLayout来实现布局的扁平化
 *
 * 查看渲染情况的工具：
 * Hierarchy Viewer
 */
public class XXXRelativeLayout extends RelativeLayout {
    public XXXRelativeLayout(Context context) {
        super(context);
    }

    public XXXRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public XXXRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }
}
