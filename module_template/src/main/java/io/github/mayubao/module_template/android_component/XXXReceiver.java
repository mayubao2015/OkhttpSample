package io.github.mayubao.module_template.android_component;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

/**
 * 1.避免在onReceive()方法中执行耗时操作，如果有需要的话，可以创建IntentServcie完成，不应该创建子线程去完成
 *  onReceive()方法超过10s会抛出ANR
 * 2.避免使用隐士Intent发送广播，信息可能会被拦截,如果这个广播是本地应用用的话，可以考虑使用LocalBroadcastManager来发送广播，避免敏感信息外泄和Intent拦截的风险
 * 3.应用内的广播的话，使用LocalBroadcastManager来注册和发送，性能更好，安全性更高
 * 4.广播的register()与unregister()应该成对出现，不然会出现内存泄漏
 */
public class XXXReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast

//        LocalBroadcastManager.getInstance(this).registerReceiver();
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
