# Android Bitmap，Drawable 与动画

## 加载大图片或者一次性加载多张图片，应该在异步线程中去执行。图片的加载,涉及到IO操作，以及CPU密集操作，很有可能引起卡顿
## ListView, ViewPager, RecycleView, GridView等组件中使用图片的，应该做好图片的缓存，避免始终持有图片导致内存溢出，也避免重复创建图片，引起性能问题
## 建议使用Fresco或者Glide
## PNG图片使用TinyPNG或者类似工具压缩处理，减少包体积（可以通过插件化的方式减少APk包的大小）
## 根据实际需要，压缩图片，而不是直接显示原图，手机屏幕比较小直接显示原图，并不会增加视觉上的收益，反而会浪费大量的内存
## 使用完毕的图片， 应该及时回收，释放宝贵的内存
## Activity#onPause()或者Activity#onStop()回掉中，关闭当前Activity中正在执行的动画
## Activity的重要生命周期方法中判断对象是否为空

## 使用RGB_565代替RGB_888,在不怎么降低视觉效果的基础上，减少内存使用
## 尽量减少Bitmap的使用，尽量使用纯色（ColorDrawable），渐变色（GradientDrawable），StateSelector(StateListDrawable)等于Shape结合的形式构建绘图
## 谨慎使用gif图片
## 大图片资源不要打包到apk中去，可以考虑通过文件仓库远程下载，减少包体积
## 当View Animation执行结束时，调用View.clearAnimation()释放相关资源















