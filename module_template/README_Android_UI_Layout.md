# Android UI与布局 规范

## 不得已多重嵌套ViewGroup的时候，不要使用LinearLayout嵌套，尽量使用RelativeLayout，有限降低嵌套层数
## Activity中弹出对话框或者浮层的时候，尽量使用DialogFrament, DialogFragment伴随Activity的生命周期
## 统一使用UTF-8
## 禁止非UI线程进行View的相关操作
## 文本大小使用单位使用dp, View的大小单位使用dp, 为了更好的还原UI, dp是受屏幕密度影响的
## 灵活使用布局，推荐使用merge,ViewStub来优化，尽量减少UI布局层级，推荐使用FrameLayout,而RelativeLayout, LinearLayout次之
## 局部刷新某一区域的组件时，避免引起全局layout刷新
    1)设置固定的View的宽高大小，如倒计时组件
    2)调用View的layout方法修改位置，如弹幕组件
    3）使用Canvas的invalidate(int l , int t, int r, int b)限定刷新区域
    4）设置一个是否允许requestLayout的变量，然后重写requestLayout, onSizeChanged方法
    根据变量控制是否requestLayout.
## 不能在Activity完全没有显示时显示Dialog或者PopupWindow
## 尽量不要使用AnimationDrawable，初始化的时候会把所有的图片加载到内存，特别占内存，并且不能释放
## 不能使用SrcollView包裹List/GridView/ExpandableListView
    因为这样会把ListView所有的Item都加载到内存中，消耗巨大的内存和Cpu去绘制图画
    注：ScrollView中嵌套ListView或者RecycleView的做法官方明确禁止
    ，如果要这么做的话，可以使用NestedScrollView

## 不要在Application中缓存数据，可以使用SharedPreference来实现数据的持久化
## Toast，每一次Toast都会创建一个Toast,建议全局使用一个Toast
## 使用Adapter时，对每一个Item的布局设置时，由于可能使用的缓存，尽量恢复现场。避免内容出现错乱















