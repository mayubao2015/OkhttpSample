package com.sensetime.senseface.db.dao;
import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.sensetime.senseface.db.DatabaseHelper;
import com.sensetime.senseface.db.entity.UserBean;

import java.sql.SQLException;
import java.util.List;

/**
 * UserDao
 */
public class UserDao {
    private Context context;
    //see the doc, and dao can operate the data from the table
    private Dao<UserBean, Integer> dao;

    public UserDao(Context context) {
        this.context = context;
        try {
            this.dao = DatabaseHelper.getInstance(context).getDao(UserBean.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // 向user表中添加一条数据
    public void insert(UserBean data) {
        try {
            dao.create(data);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // 删除user表中的一条数据
    public void delete(UserBean data) {
        try {
            dao.delete(data);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // 修改user表中的一条数据
    public void update(UserBean data) {
        try {
            dao.update(data);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // 查询user表中的所有数据
    public List<UserBean> selectAll() {
        List<UserBean> users = null;
        try {
            users = dao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }

    // 根据ID取出用户信息
    public UserBean queryById(int id) {
        UserBean user = null;
        try {
            user = dao.queryForId(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }
}