package com.sensetime.senseface.db;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.sensetime.senseface.db.entity.ArticleBean;
import com.sensetime.senseface.db.entity.UserBean;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * database manager util
 * <p>
 * the class DatabaseHelper is inherited from OrmLiteSqliteOpenHelper，create two method and override them, eg onCreate() and onUpgrade()
 * You can use method createTable() in class TableUtils to init the table in DatabaseHelper.oncreate();
 * You can use method onUpgrade() in class DatabaseHelper to upgrade your database ;
 * <p>
 * This class only have a single instance.
 * <p>
 *
 * This class hold the all DAO object in a Map, only when first call this dao, it can create itself, and then put it to the map.
 * And when not the first call, it get from the map.
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {
    // database name
    public static final String DATABASE_NAME = "sensefacedb.db";
    // database version code
    public static final int DATABASE_VERSION_CODE = 1;

    // the single instance for this database helper
    private static DatabaseHelper instance;

    // hold the all dao ,eg UserDao
    private Map<String, Dao> daos = new HashMap<>();

    // get the single instance for DatabaseHelper
    public static synchronized DatabaseHelper getInstance(Context context) {
        if (instance == null) {
            synchronized (DatabaseHelper.class) {
                if (instance == null) {
                    instance = new DatabaseHelper(context);
                }
            }
        }
        return instance;
    }

    private DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION_CODE);
    }

    // 根据传入的DAO的路径获取到这个DAO的单例对象（要么从daos这个Map中获取，要么新创建一个并存入daos）
    public synchronized Dao getDao(Class clazz) throws SQLException {
        Dao dao = null;
        String className = clazz.getSimpleName();
        if (daos.containsKey(className)) {
            dao = daos.get(className);
        }
        if (dao == null) {
            dao = super.getDao(clazz);
            daos.put(className, dao);
        }
        return dao;
    }

    @Override // 创建数据库时调用的方法
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            //TODO create the tables
            TableUtils.createTable(connectionSource, UserBean.class);
            TableUtils.createTable(connectionSource, ArticleBean.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            //TODO drop the tables
            TableUtils.dropTable(connectionSource, UserBean.class, true);
            TableUtils.dropTable(connectionSource, ArticleBean.class, true);
            onCreate(database, connectionSource);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void close() {
        for (String key : daos.keySet()) {
            Dao dao = daos.get(key);
            dao = null;
        }
        super.close();
    }
}