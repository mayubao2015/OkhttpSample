package com.sensetime.senseface.db.dao;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.sensetime.senseface.db.DatabaseHelper;
import com.sensetime.senseface.db.entity.ArticleBean;

import java.sql.SQLException;
import java.util.List;

/**
 * ArticleDao
 */
public class ArticleDao {
    private Context context;
    //see the doc, and dao can operate the data from the table
    private Dao<ArticleBean, Integer> dao;

    public ArticleDao(Context context) {
        this.context = context;
        try {
            this.dao = DatabaseHelper.getInstance(context).getDao(ArticleBean.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // 添加数据
    public void insert(ArticleBean data) {
        try {
            dao.create(data);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // 删除数据
    public void delete(ArticleBean data) {
        try {
            dao.delete(data);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // 修改数据
    public void update(ArticleBean data) {
        try {
            dao.update(data);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // 通过ID查询一条数据
    public ArticleBean queryById(int id) {
        ArticleBean article = null;
        try {
            article = dao.queryForId(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return article;
    }

    // 通过条件查询文章集合（通过用户ID查找）
    public List<ArticleBean> queryByUserId(int user_id) {
        try {
            return dao.queryBuilder().where().eq(ArticleBean.COLUMNNAME_USER, user_id).query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}