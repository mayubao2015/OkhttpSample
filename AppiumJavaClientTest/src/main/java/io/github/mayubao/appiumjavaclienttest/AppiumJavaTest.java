package io.github.mayubao.appiumjavaclienttest;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import io.appium.java_client.android.AndroidDriver;

public class AppiumJavaTest {
    public static void main(String[] args) throws MalformedURLException, InterruptedException {
        System.out.println("hello world, AppiumJavaTest");

        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("deviceName", "MI 6");
        capabilities.setCapability("automationName", "Appium");
        capabilities.setCapability("platformName", "Android");
//        capabilities.setCapability("platformVersion", "6.0");
//        capabilities.setCapability("platformVersion", "4.4.2");

//        capabilities.setCapability("appPackage", "com.android.calculator2");
//        capabilities.setCapability("appActivity", ".Calculator");
//        io.github.mayubao.okhttpsample
//        .MainActivity
        capabilities.setCapability("appPackage", "io.github.mayubao.okhttpsample");
        capabilities.setCapability("appActivity", ".MainActivity");

        AndroidDriver driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);

        String curActivityName = driver.currentActivity();
        System.out.println(curActivityName);
        List<WebElement> webElements = driver.findElements(By.className("android.widget.Button"));
        webElements.get(1).click();
        System.out.println(webElements.get(1).getText());

//        driver.findElement(By.name("Test Okhttp")).click();
//        driver.findElement(By.name("5")).click();
//        driver.findElement(By.name("9")).click();
//        driver.findElement(By.name("delete")).click();
//        driver.findElement(By.name("+")).click();
//        driver.findElement(By.name("6")).click();
//        driver.findElement(By.name("=")).click();
        Thread.sleep(2000);

//        String result = driver.findElement(By.id("com.android.calculator2:id/formula")).getText();
//        System.out.println(result);

//        driver.quit();
    }
}
