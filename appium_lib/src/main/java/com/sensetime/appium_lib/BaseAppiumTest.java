package com.sensetime.appium_lib;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;

import io.appium.java_client.android.AndroidDriver;

/**
 * 所有的测试用例类都可以继承 BaseAppiumTest。
 *
 * 优点：
 * 1.封装常见的Appium 的API， 可以链式调用
 * 2.抽取共有部分封装起来，使测试用例更加简洁
 */
public class BaseAppiumTest {

    Config config;         //Appium Client Config
    AndroidDriver driver;  //Appium Android Driver 相当于持有一个远程设备

    public Config getConfig() {
        return config;
    }

    public void setConfig(Config config) {
        this.config = config;
    }

    public AndroidDriver getDriver() {
        return driver;
    }

    public void setDriver(AndroidDriver driver) {
        this.driver = driver;
    }

    static class Config {
        String automationName   = "Appium";                 //default
        String platformName     = "Android";                //default
        String platformVersion  = "";                       //change

        String deviceName       = "Android Emulator";       //change
        String app              = "";                       //change
        String appPackage       = "";                       //change
        String appActivity      = "";                       //change

        String url              = "http://127.0.0.1:4723/wd/hub";                       //change
    }

    public Config getDefaultConfig(){
        Config config = new Config();
        config.automationName   = "Appium";
        config.platformName     = "Android";
        config.platformVersion  = "";

        config.deviceName       = "Android Emulator";
        config.app              = "Appium";
        config.appPackage       = "io.appium.android.apis";
        config.appActivity      = ".ApiDemos";

        config.url              = "http://127.0.0.1:4723/wd/hub";
        return config;
    }

    /**
     * 初始化
     */
    public void init(){
        Config config = getConfig();
        if(config == null){         //if config null, get the default config
            config = getDefaultConfig();
        }

        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("automationName", config.automationName);
        capabilities.setCapability("platformName", config.platformName );
        if(null != config.platformVersion && !"".equals(config.platformVersion.trim())){
            capabilities.setCapability("platformVersion", config.platformVersion);
        }
        capabilities.setCapability("deviceName", config.deviceName);
        capabilities.setCapability("appPackage", config.appPackage);
        capabilities.setCapability("appActivity", config.appActivity);

        try {
            driver = new AndroidDriver(new URL(config.url), capabilities);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            log("***>>>not found the driver : " + e.toString());
        }

        log("***>>>init...");
    }

    /**
     * 结束销毁
     *
     */
    public void destroy(){
        if(null != driver){
            driver.quit();
            driver = null;
        }
    }

    /**
     * 简单打印日志
     * @param msg
     */
    private void log(String msg){
        System.out.println(msg);
    }

    /**
     * 查找对应的ID的元素， 次之吵着对应文本的元素，再次之查找对应ClassName的元素
     *
     * @param id
     * @param content
     * @param className
     * @return
     */
    public WebElement findElement(String id, String content, String className){
        if(null == driver){
            return null;
        }

        WebElement element = null;
        if((element = driver.findElement(By.id(id))) != null){              //优先查找ID
            return element;
        }else if((element = driver.findElement(By.name(content))) != null){//再次查找文本
            return element;
        }else if((element = driver.findElement(By.className(className))) != null){  //次之查找ClassName
            return element;
        }
        return element;
    }


    //TODO 封装常见
    //封装常见的点击，填写文本，清楚文本，提交，滑动，休眠
}
