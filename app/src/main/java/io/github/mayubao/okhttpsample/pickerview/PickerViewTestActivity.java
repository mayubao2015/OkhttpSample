package io.github.mayubao.okhttpsample.pickerview;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bigkoo.pickerview.builder.OptionsPickerBuilder;
import com.bigkoo.pickerview.builder.TimePickerBuilder;
import com.bigkoo.pickerview.listener.OnOptionsSelectListener;
import com.bigkoo.pickerview.listener.OnTimeSelectListener;
import com.bigkoo.pickerview.view.OptionsPickerView;
import com.bigkoo.pickerview.view.TimePickerView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.github.mayubao.okhttpsample.R;

public class PickerViewTestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_picker_view_test);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//
//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

        //时间选择器
        TimePickerView pvTime = new TimePickerBuilder(this, new OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {
                Toast.makeText(PickerViewTestActivity.this, date.getTime()+"", Toast.LENGTH_SHORT).show();
            }
        }).build();

//        ((LinearLayout)findViewById(R.id.contentView)).addView(pvTime);
    }

    public void onClick(View view){
        //条件选择器
        final List<String> options1Items = new ArrayList<>();
        final List<String> options2Items = new ArrayList<>();
        final List<String> options3Items = new ArrayList<>();
        addDataList(options1Items);
        addDataList(options2Items);
        addDataList(options3Items);

        OptionsPickerView<String> pvOptions = new OptionsPickerBuilder(this, new OnOptionsSelectListener() {
            @Override
            public void onOptionsSelect(int options1, int option2, int options3 ,View v) {
                //返回的分别是三个级别的选中位置
                String tx = options1Items.get(options1)
                        + options2Items.get(option2)
                        + options3Items.get(options3);
                Log.i("OptionsPickerView", "--->>>" + tx);
            }
        }).build();
        pvOptions.setNPicker(options1Items, options2Items, options3Items);
        pvOptions.show();
    }

    private void addDataList(List<String> options){
        for(int i=0; i < 20; i++){
            options.add(i + "");
        }

    }

}
