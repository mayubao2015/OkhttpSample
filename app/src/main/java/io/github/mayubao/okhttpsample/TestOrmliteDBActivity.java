package io.github.mayubao.okhttpsample;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.j256.ormlite.dao.ForeignCollection;
import com.orhanobut.logger.CsvFormatStrategy;
import com.orhanobut.logger.DiskLogAdapter;
import com.orhanobut.logger.FormatStrategy;
import com.orhanobut.logger.Logger;

import java.util.Date;
import java.util.Iterator;

import io.github.mayubao.okhttpsample.db.dao.ArticleDao;
import io.github.mayubao.okhttpsample.db.dao.UserDao;
import io.github.mayubao.okhttpsample.db.entity.ArticleBean;
import io.github.mayubao.okhttpsample.db.entity.UserBean;
import io.github.mayubao.okhttpsample.log.LogManager;

public class TestOrmliteDBActivity extends AppCompatActivity {

    private static final String TAG = TestOrmliteDBActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_ormlite_db);

        getContentResolver();
    }


    public void click(View view){
        switch (view.getId()){
            case R.id.btn_test_insert:
                Log.i(TAG, "R.id.btn_test_insert------>>>click");
                testInsert();
                break;
            case R.id.btn_test_query:
                Log.i(TAG, "R.id.btn_test_query------>>>click");
                testQuery();
                break;
        }
    }

    /**
     * 测试查询
     */
    private void testQuery() {
        StringBuilder contentBuffer = new StringBuilder();
        // 从数据库中根据ID取出文章信息
        ArticleBean articleBean = new ArticleDao(TestOrmliteDBActivity.this).queryById(1);
        contentBuffer.append(articleBean.toString());
        // 根据取出的用户id查询用户信息
        UserBean userBean = new UserDao(TestOrmliteDBActivity.this).queryById(articleBean.getUser().getId());
        contentBuffer.append("\n\n" + userBean.toString());
        // 从用户信息中取出关联的文章列表信息
        ForeignCollection<ArticleBean> articles = userBean.getArticles();
        Iterator<ArticleBean> iterator = articles.iterator();
        contentBuffer.append("\n\n");
        while (iterator.hasNext()) {
            ArticleBean article = iterator.next();
            contentBuffer.append(article.toString() + "\n");
        }


        Log.i(TAG, "testQuery------>>>" + contentBuffer.toString());
    }

    /**
     * 测试插入
     */
    private void testInsert() {
        // 添加用户数据
        UserBean userData = new UserBean("张三", '1', new Date(), "北京");
        new UserDao(TestOrmliteDBActivity.this).insert(userData);
        // 添加文章数据
        ArticleBean articleData = new ArticleBean("标题", "内容内容内容内容内容内容", userData);
        new ArticleDao(TestOrmliteDBActivity.this).insert(articleData);
    }

}
