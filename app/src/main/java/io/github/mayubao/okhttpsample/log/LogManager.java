package io.github.mayubao.okhttpsample.log;

import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;

/**
 * Log管理类
 */
public class LogManager {

    public static void testLog(){
        Logger.addLogAdapter(new AndroidLogAdapter());
        Logger.d("testLog--->>>");
        testLog2();
    }


    public static void testLog2(){
        Logger.addLogAdapter(new AndroidLogAdapter());
        Logger.d("testLog2--->>>");
    }


    /**
     * LogManager初始化
     */
    public static void init(){
        Logger.addLogAdapter(new AndroidLogAdapter());
        Logger.addLogAdapter(new AndroidLogAdapter());
    }
}
