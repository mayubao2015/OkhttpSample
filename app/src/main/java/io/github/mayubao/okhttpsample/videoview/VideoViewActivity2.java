package io.github.mayubao.okhttpsample.videoview;

import android.content.pm.ActivityInfo;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import io.github.mayubao.okhttpsample.R;

public class VideoViewActivity2 extends AppCompatActivity {

    private static final String TAG = VideoViewActivity2.class.getSimpleName();

    VideoView mVideoView;
    LinearLayout ll_content;
    RelativeLayout rl_video;
    RelativeLayout rl_bottom;

    int videoHeight = 0;

    NestedScrollView sv_content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_view2);


        ll_content = findViewById(R.id.ll_content);
        sv_content = findViewById(R.id.sv_content);
        rl_video = findViewById(R.id.rl_video);
        rl_bottom = findViewById(R.id.rl_bottom);

        for(int i=0; i < 20; i++){
            TextView tv = new TextView(this);
            tv.setText("Hello World " + i);
            tv.setPadding(20, 20, 20, 20);
            ll_content.addView(tv);
        }

//        ll_content.setVisibility(View.GONE);
//        ll_content.setTag();
        setupVideoView();

        videoHeight = rl_video.getHeight();


    }

    public void click(View view){
//        if(screenState == PORT_SCREEN){
//            convertToLandScreen();
//        }else{
//            convertToPortScreen();
//        }

    }

    private void setupVideoView() {
        mVideoView = findViewById(R.id.video_view);


        mVideoView.setMediaController(new MediaController(this));
        mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mVideoView.start();
            }
        });
        mVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                stopPlaybackVideo();
                mVideoView.start();
            }
        });
        mVideoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                stopPlaybackVideo();
                return true;
            }
        });

        try {
            Uri uri = Uri.parse("android.resource://" + getPackageName() + "/raw/" + R.raw.test);
            mVideoView.setVideoURI(uri);
            mVideoView.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (!mVideoView.isPlaying()) {
            mVideoView.resume();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mVideoView.canPause()) {
            mVideoView.pause();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopPlaybackVideo();
    }

    private void stopPlaybackVideo() {
        try {
            mVideoView.stopPlayback();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void convertToPortScreen(){
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);//设置videoView竖屏播放
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, getResources().getDimensionPixelSize(R.dimen.dp320));

        //params.addRule(RelativeLayout.CENTER_IN_PARENT);
        mVideoView.setLayoutParams(params);

        ViewGroup.LayoutParams lp = rl_bottom.getLayoutParams();
        lp.width = 1080;
        lp.height = getResources().getDimensionPixelSize(R.dimen.dp56);
        rl_bottom.setLayoutParams(lp);

        screenState = PORT_SCREEN;
    }


    private void convertToLandScreen() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);//设置videoView全屏播放
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);//设置videoView横屏播放
        WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        android.view.Display display = wm.getDefaultDisplay();
        Point point = new Point();
        int API_LEVEL = Build.VERSION.SDK_INT;
        if (API_LEVEL >= 17) {
            display.getRealSize(point);
        } else {
            display.getSize(point);
        }
        int height = point.y;
        int width = point.x;
        Log.i(TAG, "screenHeight = " + height + " ; screenWidth = " + width);
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mVideoView
                .getLayoutParams(); // 取控当前的布局参数
        layoutParams.height = height;
        layoutParams.width = width;
        layoutParams.setMargins(0, 0, 0, 0);
        mVideoView.setLayoutParams(layoutParams);

        ViewGroup.LayoutParams lp = rl_bottom.getLayoutParams();
        lp.width = 0;
        lp.height = 0;
        rl_bottom.setLayoutParams(lp);

        screenState = LAND_SCREEN;
    }

    private int screenState = PORT_SCREEN;

    public static int  PORT_SCREEN = 0;
    public static int  LAND_SCREEN = 1;

}
