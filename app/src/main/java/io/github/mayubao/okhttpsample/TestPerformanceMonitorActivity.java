package io.github.mayubao.okhttpsample;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.github.moduth.blockcanary.BlockCanaryInternals;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * 测试性能监控的例子
 */
public class TestPerformanceMonitorActivity extends AppCompatActivity {

    private static final String TAG = TestPerformanceMonitorActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_performance_monitor);
    }


    public void click(View view){
        switch (view.getId()){
            case R.id.btn_test_performance_monitor:
                Log.i(TAG, "R.id.btn_test_performance_monitor------>>>click");
                testPerformanceMonitor();
                break;
            case R.id.btn_test_hook_blockinfo:
                Log.i(TAG, "R.id.btn_test_hook_blockinfo------>>>click");
                testHookBlockInfo();
                break;
        }
    }

    /**
     * Hook Block Info
     */
    private void testHookBlockInfo() {
//        BlockCanaryInternals.
        Exception ex = null;
        try {

            //1.get the BlockCanaryInternals object
            Class clazz = Class.forName("com.github.moduth.blockcanary.BlockCanaryInternals");
            Method method = clazz.getDeclaredMethod("getInstance", null);
            method.setAccessible(true);
            BlockCanaryInternals blockCanaryInternals = (BlockCanaryInternals) method.invoke(null, null);
            if(null != blockCanaryInternals){
                Log.i(TAG, "blockCanaryInternals------>>>" + blockCanaryInternals.toString());
            }

            //2.hook the addBlockInterceptor method to add block interceptor
//            void addBlockInterceptor(BlockInterceptor blockInterceptor)

//            Method addBlockInterceptorMethod = clazz.getDeclaredMethod("addBlockInterceptor", );

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            ex = e;
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            ex = e;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            ex = e;
        } catch (InvocationTargetException e) {
            e.printStackTrace();
            ex = e;
        }

        if(null != ex){
            Log.i(TAG, "blockCanaryInternals has exception------>>>" + ex.toString());
        }

    }

    /**
     * 测试性能监控的Demo
     */
    private void testPerformanceMonitor() {
        Log.i(TAG, "testPerformanceMonitor in main thread------>>>start");
        try {
            Thread.sleep(10 * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Log.i(TAG, "testPerformanceMonitor in main thread------>>>end");
    }
}
