package io.github.mayubao.okhttpsample.videoview;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.xiao.nicevideoplayer.NiceVideoPlayer;
import com.xiao.nicevideoplayer.NiceVideoPlayerManager;
import com.xiao.nicevideoplayer.TxVideoPlayerController;

import io.github.mayubao.okhttpsample.R;

public class ScrollViewVideoviewActivity extends AppCompatActivity {

//    VideoView mVideoView;
    LinearLayout ll_content;
    RelativeLayout rl_video;
    RelativeLayout rl_bottom;
    RelativeLayout rl_content;

    NiceVideoPlayer mNiceVideoPlayer;

    android.support.v4.widget.NestedScrollView sv_content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scroll_view_videoview);

        ll_content = findViewById(R.id.ll_content);
        sv_content = findViewById(R.id.sv_content);
        rl_video = findViewById(R.id.rl_video);
        rl_bottom = findViewById(R.id.rl_bottom);
        rl_content = findViewById(R.id.rl_content);

        mNiceVideoPlayer = findViewById(R.id.video_view);

        for(int i=0; i < 20; i++){
            TextView tv = new TextView(this);
            tv.setText("Hello World " + i);
            tv.setPadding(20, 20, 20, 20);
            ll_content.addView(tv);
        }

//        ll_content.setVisibility(View.GONE);
//        ll_content.setTag();
        setupVideoView();

    }


    private void setupVideoView() {
        /*
        mVideoView = findViewById(R.id.video_view);


//        mVideoView.setMediaController(new MediaController(this));
        mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mVideoView.start();
            }
        });
        mVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                stopPlaybackVideo();
                mVideoView.start();
            }
        });
        mVideoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                stopPlaybackVideo();
                return true;
            }
        });

        try {
            Uri uri = Uri.parse("android.resource://" + getPackageName() + "/raw/" + R.raw.test);
            mVideoView.setVideoURI(uri);
            mVideoView.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
        */

//        String mVideoUrl = "http://play.g3proxy.lecloud.com/vod/v2/MjUxLzE2LzgvbGV0di11dHMvMTQvdmVyXzAwXzIyLTExMDc2NDEzODctYXZjLTE5OTgxOS1hYWMtNDgwMDAtNTI2MTEwLTE3MDg3NjEzLWY1OGY2YzM1NjkwZTA2ZGFmYjg2MTVlYzc5MjEyZjU4LTE0OTg1NTc2ODY4MjMubXA0?b=259&mmsid=65565355&tm=1499247143&key=f0eadb4f30c404d49ff8ebad673d3742&platid=3&splatid=345&playid=0&tss=no&vtype=21&cvid=2026135183914&payff=0&pip=08cc52f8b09acd3eff8bf31688ddeced&format=0&sign=mb&dname=mobile&expect=1&tag=mobile&xformat=super";
        String mVideoUrl = "http://jzvd.nathen.cn/c6e3dc12a1154626b3476d9bf3bd7266/6b56c5f0dc31428083757a45764763b0-5287d2089db37e62345123a1be272f8b.mp4";
        mNiceVideoPlayer.setPlayerType(NiceVideoPlayer.TYPE_IJK); // or NiceVideoPlayer.TYPE_NATIVE
        mNiceVideoPlayer.setUp(mVideoUrl, null);

        /**
        TxVideoPlayerController controller = new TxVideoPlayerController(this);
        controller.setTitle("");
        controller.findViewById(R.id.image).setBackground(null);
//        controller.setImage(R.drawable.ic_player_back);
        mNiceVideoPlayer.setController(controller);
         */

        STVideoPlayerController controller = new STVideoPlayerController(this);
        controller.setTitle("");
        controller.findViewById(R.id.image).setBackground(null);
//        controller.setImage(R.drawable.ic_player_back);
        mNiceVideoPlayer.setController(controller);
    }



    @Override
    protected void onResume() {
        super.onResume();
//        if (!mVideoView.isPlaying()) {
//            mVideoView.resume();
//        }
    }

    @Override
    protected void onPause() {
        super.onPause();
//        if (mVideoView.canPause()) {
//            mVideoView.pause();
//        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopPlaybackVideo();
    }


    @Override
    protected void onStop() {
        super.onStop();
        // 在onStop时释放掉播放器
        NiceVideoPlayerManager.instance().releaseNiceVideoPlayer();
    }
    @Override
    public void onBackPressed() {
        // 在全屏或者小窗口时按返回键要先退出全屏或小窗口，
        // 所以在Activity中onBackPress要交给NiceVideoPlayer先处理。
        if (NiceVideoPlayerManager.instance().onBackPressd()) return;
        super.onBackPressed();
    }

    private void stopPlaybackVideo() {
//        try {
//            mVideoView.stopPlayback();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }




}
