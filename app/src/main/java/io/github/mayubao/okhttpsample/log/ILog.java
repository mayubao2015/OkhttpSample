package io.github.mayubao.okhttpsample.log;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * SenseFace的Log接口
 */
public interface ILog {

    void log(int priority, @Nullable String tag, @Nullable String message, @Nullable Throwable throwable);

    void d(@NonNull String message, @Nullable Object... args);

    void e(@NonNull String message, @Nullable Object... args);

    void e(@Nullable Throwable throwable, @NonNull String message, @Nullable Object... args);

    void i(@NonNull String message, @Nullable Object... args);

    void v(@NonNull String message, @Nullable Object... args);

    void w(@NonNull String message, @Nullable Object... args);

    /**
     * Tip: Use this for exceptional situations to log
     * ie: Unexpected errors etc
     */
    void wtf(@NonNull String message, @Nullable Object... args);

    /**
     * Formats the given json content and print it
     */
    void json(@Nullable String json);

    /**
     * Formats the given xml content and print it
     */
    void xml(@Nullable String xml);

    /**
     * 新增自定义的log方法
     *
     * @param type
     * @param args
     */
    void log(int type, @Nullable Object... args);
}
