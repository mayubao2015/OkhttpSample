package io.github.mayubao.okhttpsample;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import io.github.mayubao.okhttpsample.log.SFLog;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * Test Rxjava的Demo
 */
public class TestRxjavaActivity extends AppCompatActivity {

    private static final String TAG = TestRxjavaActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_rxjava);
    }


    public void click(View view){
        switch (view.getId()){
            case R.id.btn_test_normal:
                Log.i(TAG, "R.id.btn_test_normal------>>>click");
                testRxjava();
                break;
//            case R.id.btn_test_disk_log:
//                Log.i(TAG, "R.id.btn_test_disk_log------>>>click");
//                testDiskLogger();
//                break;
        }
    }

    /**
     *  testRxjava
     */
    private void testRxjava() {
        //1.创建被观察者
//        String[] items = new String[]{"one" , "two", "three" };
        Observable observable = Observable.just("one" , "two", "three");
        //2.创建观察者
        Observer<String> observer = new Observer<String>() {
            @Override
            public void onSubscribe(Disposable d) {
                log("onSubscribe--->>>");
            }

            @Override
            public void onNext(String s) {
                log("onNext--->>>" + s);
            }

            @Override
            public void onError(Throwable e) {
                log("onError--->>>");
            }

            @Override
            public void onComplete() {
                log("onComplete--->>>");
            }
        };
        observable.subscribe(observer);
    }


    /**
     * 简单打印日志
     * @param msg
     */
    public void log(String msg){
        Log.i(TAG, msg);
    }

}
