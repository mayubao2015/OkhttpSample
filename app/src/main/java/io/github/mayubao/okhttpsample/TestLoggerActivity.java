package io.github.mayubao.okhttpsample;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.CsvFormatStrategy;
import com.orhanobut.logger.DiskLogAdapter;
import com.orhanobut.logger.FormatStrategy;
import com.orhanobut.logger.Logger;

import io.github.mayubao.okhttpsample.log.LogManager;
import io.github.mayubao.okhttpsample.log.SFLog;

public class TestLoggerActivity extends AppCompatActivity {

    private static final String TAG = TestLoggerActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_logger);
    }


    public void click(View view){
        switch (view.getId()){
            case R.id.btn_test_normal:
                Log.i(TAG, "R.id.btn_test------>>>click");
                testLogger();
                break;
            case R.id.btn_test_disk_log:
                Log.i(TAG, "R.id.btn_test_disk_log------>>>click");
                testDiskLogger();
                break;
        }
    }

    /**
     * 测试DiskLogger
     */
    private void testDiskLogger() {

        FormatStrategy formatStrategy = CsvFormatStrategy.newBuilder()
                .tag("custom")
                .build();
        Logger.addLogAdapter(new DiskLogAdapter(formatStrategy));
        Logger.d("debug testDiskLogger");
        Logger.e("error testDiskLogger");
    }


    /**
     *  测试Logger框架
     */
    private void testLogger() {
//        Logger.addLogAdapter(new AndroidLogAdapter());
//        Logger.d("hello");
//
//        Logger.d("debug");
//        Logger.e("error");
//        Logger.w("warning");
//        Logger.v("verbose");
//        Logger.i("information");
//        Logger.wtf("What a Terrible Failure");

//        LogManager.testLog();
        SFLog.getInstance().d("SFLog.getInstance()--->>>debug");
    }


}
