package io.github.mayubao.okhttpsample.videoview;

import android.content.pm.ActivityInfo;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.SparseArray;
import android.util.SparseLongArray;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.VideoView;

import io.github.mayubao.okhttpsample.R;

public class VideoViewActivity extends AppCompatActivity {

    private static final String TAG = VideoViewActivity.class.getSimpleName();

    VideoView mVideoView;
    LinearLayout ll_content;
    RelativeLayout rl_video;
    RelativeLayout rl_bottom;
    RelativeLayout rl_content;
    ViewGroup.LayoutParams lp;

    int videoHeight = 0;
    int scrollDy = 0;
    int lastDy = 0;

    android.support.v4.widget.NestedScrollView sv_content;

    SparseArray<View> views = new SparseArray<>();

    FrameLayout top_frame;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_view);


        ll_content = findViewById(R.id.ll_content);
        sv_content = findViewById(R.id.sv_content);
        rl_video = findViewById(R.id.rl_video);
        rl_bottom = findViewById(R.id.rl_bottom);
        rl_content = findViewById(R.id.rl_content);

        top_frame = findViewById(R.id.top_frame);

        for(int i=0; i < 20; i++){
            TextView tv = new TextView(this);
            tv.setText("Hello World " + i);
            tv.setPadding(20, 20, 20, 20);
            ll_content.addView(tv);
        }

//        ll_content.setVisibility(View.GONE);
//        ll_content.setTag();
        setupVideoView();


        /**
        sv_content.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                Log.i("NestedScrollView", String.format("onScrollChange***>>>{scrollX=%s, scrollY=%s},{oldScrollX=%s, oldScrollY=%s}", scrollX, scrollY, oldScrollX, oldScrollY));
                RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) rl_video.getLayoutParams();
                int dy = scrollY - oldScrollY;
//                if(dy > 0){         //上滑
//                    rl_content.layout(rl_content.getLeft(), rl_content.getTop()-dy,  rl_content.getRight(),rl_content.getBottom() - dy);
//                }else{              //下滑
//                    rl_content.layout(rl_content.getLeft(), rl_content.getTop()-dy,  rl_content.getRight(),rl_content.getBottom() - dy);
//                }

                scrollDy = scrollDy + dy;
                Log.i(TAG,"onScrollChange--->>>" + dy);

                if(lastDy == -dy){
                    return;
                }

                ViewCompat.offsetTopAndBottom(rl_video, -dy);
                ViewCompat.offsetTopAndBottom(sv_content, -dy);

                lastDy = dy;

                if(scrollDy <= videoHeight){ //滑动的距离小于视频高度，视频上移
//                    rl_content.layout(rl_content.getLeft(), rl_content.getTop()-dy,  rl_content.getRight(),rl_content.getBottom() - dy);
//                    RelativeLayout.LayoutParams sclp = (RelativeLayout.LayoutParams) sv_content.getLayoutParams();
//                    sclp.width = sclp.width + dy;
//                    sv_content.setLayoutParams(sclp);

                }else{                      //滑动的距离大于视频高度，视频已经被盖住，然后滚动的是ScrollView本身
//                    ViewCompat.offsetTopAndBottom(sv_content, scrollDy);
                }

                //上滑数据
//                12-28 20:13:02.949 3508-3508/io.github.mayubao.okhttpsample I/NestedScrollView: onScrollChange***>>>{scrollX=0, scrollY=2},{oldScrollX=0, oldScrollY=0}
//                12-28 20:13:02.966 3508-3508/io.github.mayubao.okhttpsample I/NestedScrollView: onScrollChange***>>>{scrollX=0, scrollY=5},{oldScrollX=0, oldScrollY=2}
//                12-28 20:13:02.983 3508-3508/io.github.mayubao.okhttpsample I/NestedScrollView: onScrollChange***>>>{scrollX=0, scrollY=9},{oldScrollX=0, oldScrollY=5}
//                12-28 20:13:03.001 3508-3508/io.github.mayubao.okhttpsample I/NestedScrollView: onScrollChange***>>>{scrollX=0, scrollY=13},{oldScrollX=0, oldScrollY=9}

                //下滑数据
//                12-28 20:13:38.131 3508-3508/io.github.mayubao.okhttpsample I/NestedScrollView: onScrollChange***>>>{scrollX=0, scrollY=70},{oldScrollX=0, oldScrollY=71}
//                12-28 20:13:38.139 3508-3508/io.github.mayubao.okhttpsample I/NestedScrollView: onScrollChange***>>>{scrollX=0, scrollY=65},{oldScrollX=0, oldScrollY=70}
//                12-28 20:13:38.155 3508-3508/io.github.mayubao.okhttpsample I/NestedScrollView: onScrollChange***>>>{scrollX=0, scrollY=57},{oldScrollX=0, oldScrollY=65}
            }
        });
         */


        videoHeight = rl_video.getHeight();

        for(int i = 0; i < ll_content.getChildCount(); i++){
            views.put(i, ll_content.getChildAt(i));

        }

    }

    public void click(View view){
        if(screenState == PORT_SCREEN){
            convertToLandScreen();
        }else{
            convertToPortScreen();
        }


        /*
        if(screenState == PORT_SCREEN){
//            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
//            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);//设置videoView竖屏播放
//            top_frame.setVisibility(View.GONE);
//            top_frame.removeAllViews();

            screenState = LAND_SCREEN;
        }else{
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);//设置videoView全屏播放
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);//设置videoView横屏播放
//            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
            ViewGroup.LayoutParams lp = views.get(0).getLayoutParams();
            lp.width = ViewGroup.LayoutParams.MATCH_PARENT;
            lp.height = ViewGroup.LayoutParams.MATCH_PARENT;
            views.get(0).setLayoutParams(lp);
            top_frame.removeAllViews();
            top_frame.addView(views.get(0));
            top_frame.setVisibility(View.VISIBLE);

            screenState = PORT_SCREEN;
        }
        */

//        View contentView = findViewById(android.R.id.content);
    }

    private void setupVideoView() {
        mVideoView = findViewById(R.id.video_view);


//        mVideoView.setMediaController(new MediaController(this));
        mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mVideoView.start();
            }
        });
        mVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                stopPlaybackVideo();
                mVideoView.start();
            }
        });
        mVideoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                stopPlaybackVideo();
                return true;
            }
        });

        try {
            Uri uri = Uri.parse("android.resource://" + getPackageName() + "/raw/" + R.raw.test);
            mVideoView.setVideoURI(uri);
            mVideoView.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (!mVideoView.isPlaying()) {
            mVideoView.resume();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mVideoView.canPause()) {
            mVideoView.pause();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopPlaybackVideo();
    }

    private void stopPlaybackVideo() {
        try {
            mVideoView.stopPlayback();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void convertToPortScreen(){
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);//设置videoView竖屏播放
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, getResources().getDimensionPixelSize(R.dimen.dp320));

        //params.addRule(RelativeLayout.CENTER_IN_PARENT);
        mVideoView.setLayoutParams(params);

        ll_content.removeAllViews();
        for(int i=0; i < views.size(); i++){
            ll_content.addView(views.get(i));
        }

        ViewGroup.LayoutParams lp = rl_bottom.getLayoutParams();
        lp.width = 1080;
        lp.height = getResources().getDimensionPixelSize(R.dimen.dp56);
        rl_bottom.setLayoutParams(lp);

        FrameLayout.LayoutParams flp = (FrameLayout.LayoutParams) rl_content.getLayoutParams();
        flp.bottomMargin = getResources().getDimensionPixelSize(R.dimen.dp56);;

        screenState = PORT_SCREEN;
    }


    private void convertToLandScreen() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);//设置videoView全屏播放
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);//设置videoView横屏播放
        WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        android.view.Display display = wm.getDefaultDisplay();
        Point point = new Point();
        int API_LEVEL = android.os.Build.VERSION.SDK_INT;
        if (API_LEVEL >= 17) {
            display.getRealSize(point);
        } else {
            display.getSize(point);
        }
        int height = point.y;
        int width = point.x;
        Log.i(TAG, "screenHeight = " + height + " ; screenWidth = " + width);
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mVideoView
                .getLayoutParams(); // 取控当前的布局参数
        layoutParams.height = height;
        layoutParams.width = width;
        layoutParams.setMargins(0, 0, 0, 0);
        mVideoView.setLayoutParams(layoutParams);

        ll_content.removeAllViews();
        ll_content.addView(rl_video);

        ViewGroup.LayoutParams lp = rl_bottom.getLayoutParams();
        lp.width = 0;
        lp.height = 0;
        rl_bottom.setLayoutParams(lp);

        FrameLayout.LayoutParams flp = (FrameLayout.LayoutParams) rl_content.getLayoutParams();
        flp.bottomMargin = 0;

        screenState = LAND_SCREEN;
    }

    private int screenState = PORT_SCREEN;

    public static int  PORT_SCREEN = 0;
    public static int  LAND_SCREEN = 1;

}
