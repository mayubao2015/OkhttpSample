package io.github.mayubao.okhttpsample.log;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;

/**
 * SenseFace的Log类
 *
 * 包含了三方库的Logger(https://github.com/orhanobut/logger)功能，也能实现自己的的Log系统的需求
 */
public class SFLog implements ILog {

    //SenseFace log message type
    public static final int TYPE_COMMON_BLOCK_INFO  = 0x0001;   //卡顿的共性问题
    public static final int TYPE_NET_INFO           = 0x0002;   //网络问题
    public static final int TYPE_EXCEPTION_INFO     = 0x0003;   //异常问题
    //其他常量的定义...

    private static SFLog mInstance = new SFLog();

    private SFLog() {
        //init log system

        //init the library logger
        Logger.addLogAdapter(new AndroidLogAdapter());
    }

    public static SFLog getInstance() {
        if (mInstance == null) {
            synchronized (SFLog.class) {
                if (mInstance == null) {
                    mInstance = new SFLog();
                }
            }
        }
        return mInstance;
    }



    @Override
    public void log(int priority, @Nullable String tag, @Nullable String message, @Nullable Throwable throwable) {
        Logger.log(priority, tag, message, throwable);
    }

    @Override
    public void d(@NonNull String message, @Nullable Object... args) {
        Logger.d(message, args);
    }

    @Override
    public void e(@NonNull String message, @Nullable Object... args) {
        Logger.e(message, args);
    }

    @Override
    public void e(@Nullable Throwable throwable, @NonNull String message, @Nullable Object... args) {
        Logger.e(throwable, message, args);
    }

    @Override
    public void i(@NonNull String message, @Nullable Object... args) {
        Logger.i(message, args);
    }

    @Override
    public void v(@NonNull String message, @Nullable Object... args) {
        Logger.v(message, args);
    }

    @Override
    public void w(@NonNull String message, @Nullable Object... args) {
        Logger.w(message, args);
    }

    @Override
    public void wtf(@NonNull String message, @Nullable Object... args) {
        Logger.wtf(message, args);
    }

    @Override
    public void json(@Nullable String json) {
        Logger.json(json);
    }

    @Override
    public void xml(@Nullable String xml) {
        Logger.xml(xml);
    }

    @Override
    public void log(int type, @Nullable Object... args) {
        //TODO do some log work accroding to sense face business
    }
}
