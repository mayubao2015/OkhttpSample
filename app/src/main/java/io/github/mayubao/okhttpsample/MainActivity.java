package io.github.mayubao.okhttpsample;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import java.io.IOException;

import io.github.mayubao.okhttpsample.pickerview.PickerViewTestActivity;
import io.github.mayubao.okhttpsample.recycleview.LoadMoreActivity;
import io.github.mayubao.okhttpsample.videoview.ScrollViewVideoviewActivity;
import io.github.mayubao.okhttpsample.videoview.VideoViewActivity;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    public void click(View view){
        switch (view.getId()){
            case R.id.btn_test:
                Log.i(TAG, "R.id.btn_test------>>>click");
                testOkhttpInterceptor();
                break;
            case R.id.btn_log: {
                Log.i(TAG, "R.id.btn_log------>>>click");
//                testLogger();
                Intent intent = new Intent(this, TestLoggerActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_performance_monitor: {
                Log.i(TAG, "R.id.btn_performance_monitor------>>>click");
//                testLogger();
                Intent intent = new Intent(this, TestPerformanceMonitorActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_test_ormlite: {
                Log.i(TAG, "R.id.btn_test_ormlite------>>>click");
//                testLogger();
                Intent intent = new Intent(this, TestOrmliteDBActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_test_rxjava: {
                Log.i(TAG, "R.id.btn_test_rxjava------>>>click");
                Intent intent = new Intent(this, TestRxjavaActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_video_view: {
                Log.i(TAG, "R.id.btn_video_view------>>>click");
                Intent intent = new Intent(this, VideoViewActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_recycle_view: {
                Log.i(TAG, "R.id.btn_recycle_view------>>>click");
                Intent intent = new Intent(this, LoadMoreActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_scrolll_video_view: {
                Log.i(TAG, "R.id.btn_scrolll_video_view------>>>click");
                Intent intent = new Intent(this, ScrollViewVideoviewActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.btn_picker_view: {
                Log.i(TAG, "R.id.btn_picker_view------>>>click");
                Intent intent = new Intent(this, PickerViewTestActivity.class);
                startActivity(intent);
                break;
            }
        }
    }

    /**
     *  测试Logger框架
     */
    private void testLogger() {
//        Logger.addLogAdapter(new AndroidLogAdapter());
//        Logger.d("hello");
//
//        Logger.d("debug");
//        Logger.e("error");
//        Logger.w("warning");
//        Logger.v("verbose");
//        Logger.i("information");
//        Logger.wtf("What a Terrible Failure");
    }


//    http://wanandroid.com/


    /**
     * 测试Okhttp拦截器
     */
    public void testOkhttpInterceptor(){
        new Thread(){
            @Override
            public void run() {
                OkHttpClient client = new OkHttpClient.Builder() .
                        addNetworkInterceptor(new LoggingInterceptor()) .build();
//                OkHttpClient client = new OkHttpClient.Builder().build();
                Request request = new Request.Builder() .url("http://wanandroid.com") .header("User-Agent", "OkHttp Example") .build();
                Response response;

                {
                    try {
                        response = client.newCall(request).execute();
                        int code = response.code();
                        long bodyLen = response.body().contentLength();

                        Log.i(TAG, String.format("Response get response code :%s ,and body length is %s", code, bodyLen));
                    } catch (IOException e) {
                        e.printStackTrace();
                        Log.i(TAG, "testOkhttpInterceptor------>>>have exception:" + e.toString());
                    }
                }

            }
        }.start();
    }


    /**
     * 日志拦截类
     */
    class LoggingInterceptor implements Interceptor {
        @Override
        public Response intercept(Interceptor.Chain chain) throws IOException {
            Request request = chain.request();
            long t1 = System.nanoTime();
            Log.i("LoggingInterceptor",String.format("Sending request %s on %s%n%s", request.url(), chain.connection(), request.headers()));
            Response response = chain.proceed(request);
            long t2 = System.nanoTime();
            Log.i("LoggingInterceptor",String.format("Received response for %s in %.1fms%n%s", response.request().url(), (t2 - t1) / 1e6d, response.headers()));
            return response;
        }
    }



}
