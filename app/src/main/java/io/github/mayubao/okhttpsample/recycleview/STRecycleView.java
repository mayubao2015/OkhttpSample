package io.github.mayubao.okhttpsample.recycleview;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import io.github.mayubao.okhttpsample.R;

/**
 * 适用与布局为LinearLayout的加载更多的RecycleView
 */
public class STRecycleView extends RecyclerView{

    /**
     * item 类型
     */
    public final static int ITEM_TYPE_FOOTER    =   2;//底部--往往是loading_more
    public final static int ITEM_TYPE_LIST      =   3;//代表item展示的模式是list模式

    private boolean mIsFooterEnable;//是否允许加载更多

    /**
     * 自定义实现了头部和底部加载更多的adapter
     */
    private LoadMoreAdapterWrapper mAutoLoadAdapter;

    /**
     * 标记是否正在加载更多，防止再次调用加载更多接口
     */
    private boolean mIsLoadingMore;
    /**
     * 标记加载更多的position
     */
    private int mLoadMorePosition;

    /**
     * 加载类型，可以为自动加载或者手动加载
     */
    private LoadType mLoadType = LoadType.AUTO_LOAD;

    private int mFooterLayoutId = R.layout.layout_load_more;

    public STRecycleView(@NonNull Context context) {
        super(context);
        init();
    }

    public STRecycleView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public STRecycleView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    /**
     * 设置加载类型
     * @param mLoadType
     */
    public void setLoadType(LoadType mLoadType) {
        this.mLoadType = mLoadType;
    }

    /**
     * 设置是否支持自动加载更多
     *
     * @param autoLoadMore
     */
    public void setAutoLoadMoreEnable(boolean autoLoadMore){
        mIsFooterEnable=autoLoadMore;
    }
    /**
     * 通知更多的数据已经加载
     * <p>
     * 每次加载完成之后添加了Data数据，用notifyItemRemoved来刷新列表展示，
     * 而不是用notifyDataSetChanged来刷新列表
     *
     * @param hasMore
     */
    public void notifyMoreFinish(boolean hasMore){
        setAutoLoadMoreEnable(hasMore);
        if (mLoadType == LoadType.AUTO_LOAD)
            getAdapter().notifyItemRemoved(mLoadMorePosition);
        else {
            getAdapter().notifyItemChanged(mLoadMorePosition + 1);
        }
        mIsLoadingMore=false;
    }

    /**
     * 设置加载更多UI资源文件
     * @param mFooterLayoutId
     */
    public void setFooterLayoutId(int mFooterLayoutId) {
        this.mFooterLayoutId = mFooterLayoutId;
    }

    /**
     * 初始化-添加滚动监听
     * 加载更多的触发条件：
     *  当前最后一条可见的view是否是当前数据列表的最后一条
     */
    private void init() {
        super.addOnScrollListener(new OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                //判断当前的最后一条可见的View是否当前数据列表的最后一条，如果是就显示加载更多，且触发加载更多的函数
                if(null != mOnLoadMoreListener && mIsFooterEnable && !mIsLoadingMore && dy >= 0){
                    int lastVisiblePosition = getLastVisiblePosition();
                    if(lastVisiblePosition + 1 == getAdapter().getItemCount()){
                        setLoadingMoreStatus(true);
                        mLoadMorePosition = lastVisiblePosition;
                        mOnLoadMoreListener.onLoadMore();
                    }
                }
            }
        });
    }

    /**
     * 设置正在加载更多
     *
     * @param loadingMore
     */
    public void setLoadingMoreStatus(boolean loadingMore){
        this.mIsLoadingMore=loadingMore;
    }

    @Override
    public void setAdapter(Adapter adapter){
        if (adapter != null) {
            mAutoLoadAdapter = new LoadMoreAdapterWrapper(adapter);
            adapter.registerAdapterDataObserver(new AdapterDataObserver(){
                @Override
                public void onChanged(){
                    mAutoLoadAdapter.notifyDataSetChanged();
                }
                @Override
                public void onItemRangeChanged(int positionStart,int itemCount){
                    mAutoLoadAdapter.notifyItemRangeChanged(positionStart,itemCount);
                }
                @Override
                public void onItemRangeChanged(int positionStart,int itemCount,Object payload){
                    mAutoLoadAdapter.notifyItemRangeChanged(positionStart,itemCount,payload);
                }
                @Override
                public void onItemRangeInserted(int positionStart,int itemCount){
                    mAutoLoadAdapter.notifyItemRangeInserted(positionStart,itemCount);
                }
                @Override
                public void onItemRangeRemoved(int positionStart,int itemCount){
                    mAutoLoadAdapter.notifyItemRangeRemoved(positionStart,itemCount);
                }
                @Override
                public void onItemRangeMoved(int fromPosition,int toPosition,int itemCount){
                    mAutoLoadAdapter.notifyItemRangeChanged(fromPosition,toPosition,itemCount);
                }
            });
        }
        super.swapAdapter(mAutoLoadAdapter,true);
    }

    /**
     * 获取最后一条展示的位置
     *
     * @return
     */
    private int getLastVisiblePosition(){
        return ((LinearLayoutManager)getLayoutManager()).findLastVisibleItemPosition();
    }

    /**
     * 加载更多监听
     */
    public interface OnLoadMoreListener{
        /**
         * 加载更多
         */
        void onLoadMore();
    }

    OnLoadMoreListener mOnLoadMoreListener;

    /**
     * 设置加载更多监听
     * @param mOnLoadMoreListener
     */
    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }


    /**
     * 加载更多的Adapter
     */
    public class LoadMoreAdapterWrapper extends Adapter<ViewHolder>{

        /**
         * 数据adapter
         */
        private Adapter mInternalAdapter;

        public LoadMoreAdapterWrapper(Adapter mInternalAdapter) {
            this.mInternalAdapter = mInternalAdapter;
        }

        @Override
        public int getItemViewType(int position){
            int footerPosition=getItemCount() - 1;
            if (footerPosition == position && mIsFooterEnable) {
                return ITEM_TYPE_FOOTER;
            } else {
                return ITEM_TYPE_LIST;
            }
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            if (viewType == ITEM_TYPE_FOOTER) {              //加载更多类型
                return new FooterViewHolder(LayoutInflater.from(parent.getContext()).inflate(mFooterLayoutId, parent,false));
            } else {
                return mInternalAdapter.onCreateViewHolder(parent,viewType);
            }
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            int type=getItemViewType(position);
            if (type != ITEM_TYPE_FOOTER) {
                mInternalAdapter.onBindViewHolder(holder,position);
            }
        }

        @Override
        public int getItemCount() {
            int count = mInternalAdapter.getItemCount();
            if (mIsFooterEnable) count++;
            return count;
        }

        /**
         * 底部的ViewHolder
         */
        public class FooterViewHolder extends ViewHolder{
            public FooterViewHolder(@NonNull final View itemView) {
                super(itemView);
                if (mLoadType != LoadType.AUTO_LOAD) {      //手动刷新
                    itemView.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (!mIsLoadingMore) {
                                setLoadingMoreStatus(true);
                                itemView.setVisibility(GONE);
                                mOnLoadMoreListener.onLoadMore();
                            }
                        }
                    });
                }
            }
        }
    }

    /**
     * 加载类型
     */
    enum LoadType{
        AUTO_LOAD,MANUAL_LOAD
    }
}
