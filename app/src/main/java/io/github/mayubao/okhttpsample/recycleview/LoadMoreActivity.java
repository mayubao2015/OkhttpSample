package io.github.mayubao.okhttpsample.recycleview;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.brooks.loadmorerecyclerview.LoadMoreRecyclerView;
import com.zhy.adapter.recyclerview.wrapper.LoadMoreWrapper;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import io.github.mayubao.okhttpsample.R;

public class LoadMoreActivity extends AppCompatActivity {

    STRecycleView recycleview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load_more);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        initRecycleView();

    }

    private void initRecycleView() {
        recycleview = findViewById(R.id.recycleview);
//        final MyAdapter mOriginAdapter = new MyAdapter(this);
//        recycleview.setLayoutManager(new LinearLayoutManager(this));
//        LoadMoreWrapper mLoadMoreWrapper = new LoadMoreWrapper(mOriginAdapter);
//        mLoadMoreWrapper.setLoadMoreView(R.layout.layout_load_more);
//        mLoadMoreWrapper.setOnLoadMoreListener(new LoadMoreWrapper.OnLoadMoreListener()
//        {
//            @Override
//            public void onLoadMoreRequested()
//            {
//                Log.i("LoadMoreActivity", "onLoadMoreRequested--->>>");
//                try {
//                    Thread.sleep(2*1000);
//                    mOriginAdapter.addDataList(getFakeDataList());
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//        });
//        recycleview.setAdapter(mOriginAdapter);


        final MyAdapter mOriginAdapter = new MyAdapter(this);
        recycleview.setLayoutManager(new LinearLayoutManager(this));
        recycleview.setAutoLoadMoreEnable(true);
        recycleview.setOnLoadMoreListener(new STRecycleView.OnLoadMoreListener(){
            @Override
            public void onLoadMore(){
                Log.i("LoadMoreActivity", "--->>>onLoadMore");
                recycleview.postDelayed(new Runnable(){
                    @Override
                    public void run(){
                        mOriginAdapter.addDataList(getFakeDataList());
                        recycleview.notifyMoreFinish(true);
                    }
                },5000);
            }
        });
        recycleview.setAdapter(mOriginAdapter);


        mOriginAdapter.addDataList(getFakeDataList());
    }


    int count = 0;
    private List<String> getFakeDataList(){
        List<String> datalist = new ArrayList<>();
        for(int i=0; i < 20; i++){
            count ++;
            datalist.add("hello world ===>>>" + count);
        }
        return datalist;
    }


    class MyAdapter extends RecyclerView.Adapter<MyViewHolder>{
        public static final int COMMON_PADDING = 30;
        private Context context;
        private List<String> dataList;

        public MyAdapter(Context context, List<String> dataList) {
            this.context = context;
            this.dataList = dataList;
        }

        public MyAdapter(Context context) {
            this.context = context;
            this.dataList = new ArrayList<>();
        }

        public void addDataList(List<String> dataList){
            this.dataList.addAll(dataList);
            notifyDataSetChanged();
        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            TextView tv = new TextView(context);
            tv.setText("Hell World");
            tv.setTextColor(getResources().getColor(R.color.colorPrimary));
            tv.setPadding(COMMON_PADDING, COMMON_PADDING, COMMON_PADDING, COMMON_PADDING);
            return new MyViewHolder(tv);
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, int i) {
            holder.tv.setText(this.dataList.get(i));
        }

        @Override
        public int getItemCount() {
            return this.dataList.size();
        }
    }


    class MyViewHolder extends RecyclerView.ViewHolder{
        TextView tv;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            if(itemView instanceof TextView){
                tv = (TextView) itemView;
            }
        }
    }

}
