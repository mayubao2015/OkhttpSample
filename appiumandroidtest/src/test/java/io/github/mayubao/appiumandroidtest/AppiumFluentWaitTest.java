package io.github.mayubao.appiumandroidtest;

import static java.time.Duration.ofSeconds;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;

import io.appium.java_client.AppiumFluentWait;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.support.ui.Sleeper;
import org.openqa.selenium.support.ui.Wait;

import java.time.Clock;
import java.time.Duration;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;

public class AppiumFluentWaitTest {

    private static class FakeElement {
        public boolean isDisplayed() {
            return false;
        }
    }

    @Test(expected = TimeoutException.class)
    public void testDefaultStrategy() {
        final FakeElement el = new FakeElement();

//        final Wait<FakeElement> wait = new AppiumFluentWait<FakeElement>(el, (org.openqa.selenium.support.ui.Clock) Clock.systemDefaultZone(), new Sleeper() {
//            @Override
//            public void sleep(Duration duration) throws InterruptedException {
//                assertThat(duration.getSeconds(), is(equalTo(1L)));
//                Thread.sleep(duration.toMillis());
//            }
//        }).withPollingStrategy(iterationInfo -> null)
//                .withTimeout(ofSeconds(3))
//                .pollingEvery(ofSeconds(1));
//        final Wait<FakeElement> wait = new AppiumFluentWait<>(el, Clock.systemDefaultZone(), duration -> {
//            assertThat(duration.getSeconds(), is(equalTo(1L)));
//            Thread.sleep(duration.toMillis());
//        }).withPollingStrategy(AppiumFluentWait.IterationInfo::getInterval)
//                .withTimeout(ofSeconds(3))
//                .pollingEvery(ofSeconds(1));


//        wait.until(fakeElement -> false);
//        wait.until(FakeElement::isDisplayed);
        Assert.fail("TimeoutException is expected");
    }

    @Test
    public void testCustomStrategyOverridesDefaultInterval() {
//        final FakeElement el = new FakeElement();
//        final AtomicInteger callsCounter = new AtomicInteger(0);
//        final Wait<FakeElement> wait = new AppiumFluentWait<>(el, Clock.systemDefaultZone(), duration -> {
//            callsCounter.incrementAndGet();
//            assertThat(duration.getSeconds(), is(equalTo(2L)));
//            Thread.sleep(duration.toMillis());
//        }).withPollingStrategy(info -> ofSeconds(2))
//                .withTimeout(ofSeconds(3))
//                .pollingEvery(ofSeconds(1));
//        try {
//            wait.until(FakeElement::isDisplayed);
//            Assert.fail("TimeoutException is expected");
//        } catch (TimeoutException e) {
//            // this is expected
//            assertThat(callsCounter.get(), is(equalTo(2)));
//        }
    }

    @Test
    public void testIntervalCalculationForCustomStrategy() {
//        final FakeElement el = new FakeElement();
//        final AtomicInteger callsCounter = new AtomicInteger(0);
//        // Linear dependency
//        final Function<Long, Long> pollingStrategy = x -> x * 2;
//        final Wait<FakeElement> wait = new AppiumFluentWait<>(el, Clock.systemDefaultZone(), duration -> {
//            int callNumber = callsCounter.incrementAndGet();
//            assertThat(duration.getSeconds(), is(equalTo(pollingStrategy.apply((long) callNumber))));
//            Thread.sleep(duration.toMillis());
//        }).withPollingStrategy(info -> ofSeconds(pollingStrategy.apply(info.getNumber())))
//                .withTimeout(ofSeconds(4))
//                .pollingEvery(ofSeconds(1));
//        try {
//            wait.until(FakeElement::isDisplayed);
//            Assert.fail("TimeoutException is expected");
//        } catch (TimeoutException e) {
//            // this is expected
//            assertThat(callsCounter.get(), is(equalTo(2)));
//        }
    }
}