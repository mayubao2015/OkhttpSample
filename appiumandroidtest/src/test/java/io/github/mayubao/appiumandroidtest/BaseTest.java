package io.github.mayubao.appiumandroidtest;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AutomationName;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServerHasNotBeenStartedLocallyException;

public class BaseTest {

    AppiumDriverLocalService service;
    AndroidDriver driver;

    /**
     * initialization.
     */
    @Before
    public void beforeClass() {
//        service = AppiumDriverLocalService.buildDefaultService();
//        service.start();
//
//        if (service == null || !service.isRunning()) {
//            throw new AppiumServerHasNotBeenStartedLocallyException(
//                    "An appium server node is not started!");
//        }
//
//        File appDir = new File("src/test/java/io/github/mayubao/appiumandroidtest");
//        File app = new File(appDir, "ApiDemos-debug.apk");
//        DesiredCapabilities capabilities = new DesiredCapabilities();
//        capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, AutomationName.ANDROID_UIAUTOMATOR2);
////        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "Android Emulator");
//        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "MI 6");
//        capabilities.setCapability(MobileCapabilityType.APP, app.getAbsolutePath());
//        driver = new AndroidDriver<>(service.getUrl(), capabilities);



//        io.appium.android.apis
//        ApiDemos

        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("deviceName", "MI 6");
        capabilities.setCapability("automationName", "Appium");
        capabilities.setCapability("platformName", "Android");
//        capabilities.setCapability("platformVersion", "6.0");
//        capabilities.setCapability("platformVersion", "4.4.2");

//        capabilities.setCapability("appPackage", "com.android.calculator2");
//        capabilities.setCapability("appActivity", ".Calculator");
//        io.github.mayubao.okhttpsample
//        .MainActivity
        capabilities.setCapability("appPackage", "io.appium.android.apis");
        capabilities.setCapability("appActivity", ".ApiDemos");

        try {
//            driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
//            http://172.20.4.47
            driver = new AndroidDriver(new URL("http://172.20.4.47:4723/wd/hub"), capabilities);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            System.out.println("***>>>not found the driver");
        }

        System.out.println("***>>>beforeClass");
    }

    /**
     * finishing.
     */
    @After
    public void afterClass() {
        if (driver != null) {
            driver.quit();
        }
        if (service != null) {
            service.stop();
        }
        System.out.println("***>>>afterClass");
    }

}
