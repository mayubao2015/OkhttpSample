package io.github.mayubao.appiumandroidtest;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.DeviceRotation;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

import io.appium.java_client.MobileBy;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.Activity;
import io.appium.java_client.android.AndroidStartScreenRecordingOptions;
import io.appium.java_client.android.GsmCallActions;

import static io.appium.java_client.touch.offset.ElementOption.element;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class AndroidDemoTest extends BaseTest{

    @Test
    public void runFirstTest(){
        System.out.println("--->>>runFirstTest");
        try {
            driver.sendSMS("11111111", "call");
        } catch (Exception e) {
            System.out.println("method works only in emulators");
        }
    }


    @Test
    public void runFirstTwo(){
        System.out.println("--->>>runFirstTwo");
        try {
            driver.makeGsmCall("11111111", GsmCallActions.CALL);
            driver.makeGsmCall("11111111", GsmCallActions.ACCEPT);
        } catch (Exception e) {
            System.out.println("method works only in emulators");
        }
    }

    @Test
    public void testLandscapeRightRotation() {
        new WebDriverWait(driver, 20).until(ExpectedConditions
                .elementToBeClickable(driver.findElementById("android:id/content")
                        .findElement(MobileBy.AccessibilityId("Graphics"))));
        DeviceRotation landscapeRightRotation = new DeviceRotation(0, 0, 90);
        driver.rotate(landscapeRightRotation);
        assertEquals(driver.rotation(), landscapeRightRotation);
    }


    @Test
    public void verifyBasicScreenRecordingWorks() throws InterruptedException {
        try {
            Activity activity = new Activity("io.appium.android.apis", ".ApiDemos");
            driver.startActivity(activity);
            driver.startRecordingScreen(
                    new AndroidStartScreenRecordingOptions()
                            .withTimeLimit(Duration.ofSeconds(5))
            );
        } catch (WebDriverException e) {
            if (e.getMessage().toLowerCase().contains("emulator")) {
                // screen recording only works on real devices
                return;
            }
        }
        Thread.sleep(5000);
        String result = driver.stopRecordingScreen();
    }

    @Test
    public void dragNDropByElementTest() {
        driver.resetApp();
        Activity activity = new Activity("io.appium.android.apis", ".view.DragAndDropDemo");
        driver.startActivity(activity);
        WebElement dragDot1 = driver.findElement(By.id("io.appium.android.apis:id/drag_dot_1"));
        WebElement dragDot3 = driver.findElement(By.id("io.appium.android.apis:id/drag_dot_3"));

        WebElement dragText = driver.findElement(By.id("io.appium.android.apis:id/drag_text"));
        assertEquals("Drag text not empty", "", dragText.getText());

        TouchAction dragNDrop = new TouchAction(driver)
                .longPress(element(dragDot1))
                .moveTo(element(dragDot3))
                .release();
        dragNDrop.perform();
        assertNotEquals("Drag text empty", "", dragText.getText());
    }
}
