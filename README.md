# OkhttpSample

#### 项目介绍
OkhttpSample 这是我的一个测试工程。起初是做Okhttp的Demo类，后来集成了很多测试，包括Logger的测试，DB的测试，APM的测试，陆续会添加更多的测试。

#### 遇到的问题

AndroidStudio构建项目的时候会遇到很多问题，问题以及解决方案列表如下：

 - 依赖的三方库在对应的仓库找不到
    修改repository，或者添加对应的仓库
    获取repository的jar或者依赖的组建自行集成到项目中去
 - 下载不了Gradle组件
    翻墙或者添加代理






